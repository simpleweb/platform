#!/bin/sh

# Script to delete (or reset) an existing application

dir=/var/simpleWeb

toDelete=${1:-test}

undo() {
	sudo rm -rf "$dir/apps/$toDelete" "$dir/conf/apache/$toDelete.conf" "/etc/apache2/sites-available/$toDelete.conf" "/etc/apache2/sites-enabled/$toDelete.conf"
}

undo
