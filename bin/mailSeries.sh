#!/bin/bash

# Send a series by mail

app='fils'
protocol='https'
domain='filsdu.net'
week=$(date -d '-4days' '+%Y%V')
from="Fils du Net <contact@$domain>"
subject="Series $week"
to="$(whoami)"
bulk=false

while getopts ':a:p:d:w:s:t:f:b:' opt; do
	case $opt in
	a) app=$OPTARG ;;
	p) protocol=$OPTARG ;;
	d) domain=$OPTARG ;;
	w) week=$OPTARG ;;
	s) subject=$OPTARG ;;
	t) to=$OPTARG ;;
	f) from=$OPTARG ;;
	b) bulk=$OPTARG ;;
	*)
		echo 'Utilisation : ./bin/mailSeries.sh [OPTION]...

You can use the following options, all optional:
  -a			"application" for which to send the mail, defaults to "fils"
  -p			"protocol" to use with the domain, defaults to "https"
  -d			"domain" from which to download the content that will be sent
  -w			"week" to send, defaults to last week, formatted as yyyyww, exemple: 201905
  -f			"from", author email address
  -s			"subject" of the email
  -t			"to", recipient email address, defaults to "whois"
  -b			"bulk" set as "true", then the visible "to" will be replaced with the "from", and meta data will be added to make it a bulk email

Exemple: ./bin/mailSeries.sh -f "test <test@exemple.com>" -s "Hello world" -t "foo bar <foo@exemple.com>"'
		exit
		;;
	esac
done

# Redirect output to logger
exec 1> >(logger -s -t "$app") 2>&1

echo "$protocol://$domain/series/$week/ => '$from' sending '$subject' to '$to'"

# Requires the debian packages "html-xml-utils", "w3m" and "tidy"
if [ ! "$(command -v hxprune)" ]; then
	echo 'The command "hxprune" is not present on your system, on Debian you can install "html-xml-utils"'
	exit
fi
if [ ! "$(command -v tidy)" ]; then
	echo 'The command "tidy" is not present on your system, on Debian you can install "tidy"'
	exit
fi

mkdir -p "/tmp/fils/$week"
[ -f "/tmp/fils/$week/index.html" ] && rm "/tmp/fils/$week/index.html"
wget -q --convert-links "$protocol://$domain/series/$week/" -P "/tmp/fils/$week/"

# Deleting unused nodes
body=$(hxprune -c bg-top "/tmp/fils/$week/index.html" |
	hxprune -c bg-black |
	hxprune -c bg-dark-gray |
	hxprune -c w-40-ns |
	hxprune -c readMore |
	hxprune -c flex-none)

# Content which is originally on 60% width is enlarged to 100%
body=${body//w-60-ns /}

mailToSend="From: $from
Subject: $subject"

if [ "$bulk" = "true" ]; then
	mailToSend="$mailToSend
To: $from
list-id: $subject <series.$domain>
list-label: $subject
list-unsubscribe: $from
Precedence: list
Auto-Submitted: auto-generated"
else
	mailToSend="$mailToSend
To: $to"
fi

mailToSend="$mailToSend
MIME-Version: 1.0
Content-Type: multipart/alternative; charset=UTF-8; boundary=\"MSGBOUNDARY\"
Content-Transfer-Encoding: 8bit

--MSGBOUNDARY
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

$(echo "$body" | w3m -cols 512 -dump -T text/html)

--MSGBOUNDARY
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 8bit

$(echo "$body" | tidy -q -utf8 -w 120)

--MSGBOUNDARY--"

echo "$mailToSend" | /usr/sbin/sendmail -f "$from" "$to"

# vim: ts=2 sw=2
