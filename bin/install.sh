#!/bin/sh

echo
echo "Let's install a 'simple web' platform!

This script will download and setup an initial configuration.

It will ask you for some credentials, in order to install some required software"

[ ! "$(command -v git)" ] && sudo apt install git
[ ! "$(command -v hugo)" ] && sudo apt install hugo

if [ ! -d "/var/simpleWeb" ]; then
	git -C /tmp clone git@git.framasoft.org:simpleWeb/platform.git simpleWeb
	sudo mv simpleWeb /var
	cd /var/simpleWeb || exit
fi

cd /var/simpleWeb || exit

if [ ! -h /etc/systemd/system/simpleWeb.service ]; then
	sudo cp /var/simpleWeb/conf/simpleWeb.service /etc/systemd/system/
	sudo systemctl enable simpleWeb
fi

if [ ! -h /etc/apache2/sites-enabled/0simpleWeb.conf ]; then
	echo
	echo "Setting up the main simple web apache configuration, and a sandbox"
	sudo ln -fs "$(pwd)/conf/apache/simpleWeb.conf" /etc/apache2/sites-available/0simpleWeb.conf
	sudo a2ensite 0simpleWeb
	if [ ! -d "/var/simpleWeb/apps/sandbox" ]; then
		echo "Get sandbox code"
		git -C /var/simpleWeb/apps clone git@git.framasoft.org:simpleWeb/sandbox.git
		echo "Get sandbox kube theme"
		git -C /var/simpleWeb/apps/sandbox submodule update --init themes/kube
	fi

	sudo ln -fs "$(pwd)/conf/apache/sandbox.conf" /etc/apache2/sites-available/sandbox.conf
	sudo a2ensite sandbox

	sudo a2enmod alias autoindex dav_fs dir headers setenvif

	sudo service apache2 restart
fi

echo

if [ -d apps ]; then
	echo "Folders and sandbox already setup"
else
	echo "Create and prepare folders"
	mkdir -p apps
	mkdir -p log/app
	mkdir -p log/apache
	mkdir -p run
	sudo chmod g+w apps
	sudo chmod -R g+w log
	sudo chmod g+w run
	sudo chgrp www-data apps
	sudo chgrp www-data log
	sudo chgrp www-data run

	echo
	echo "Generate the sandbox website"
	sudo chgrp www-data apps
	sudo chmod g+w apps
	sudo -u www-data rsync -r --delete conf/sandbox apps/
	sudo -u www-data hugo --quiet -s apps/sandbox
fi
