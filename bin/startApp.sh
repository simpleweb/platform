#!/bin/sh

# Start application, hugo (or middleman?)

app="$1"
NB_MINUTES_TIMEOUT=1

[ -f "apps/$app/bin/NB_MINUTES_TIMEOUT" ] && NB_MINUTES_TIMEOUT=$(cat "apps/$app/bin/NB_MINUTES_TIMEOUT")

# Check if application is already started
[ -f "run/$app.pid" ] && [ -d "/proc/$(cat "run/$app.pid")" ] && exit

echo "Starting application..."

# Useful in some themes like ananke
export HUGO_ENV="production"

cd "apps/$app" || exit
hugo --environment production --gc --minify --watch >public/app.log 2>&1 &
pid=$!
cd ../..
echo $pid >"run/$app.pid"

timeout() {
	current=$(date +%s)
	last_modified=$(date -r "log/apache/$app.log" +%s)
	next_check=$((last_modified - current + NB_MINUTES_TIMEOUT * 60))

	echo "Next check for process $pid should be in ${next_check} seconds"

	# To handle a failing hugo
	sleep 3

	if [ -d "/proc/$pid" ] && [ $next_check -gt 0 ]; then
		sleep $next_check
		timeout
	fi
}

timeout

echo "Killing pid $pid"
kill "$pid" && rm "run/$app.pid" || echo "Not started"
