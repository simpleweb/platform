#!/bin/sh

cd "$(dirname "$0")/.." || exit

echo "Tailing apache logs to start hugo and other apps"

for log_file in log/apache/*.log; do
	app=$(basename "$log_file")
	app=${app%.log}

	echo "Following log file for $app" >>"log/app/$app.log"

	tail -n 0 -F "log/apache/$app.log" | while read -r line; do
		status=$(echo "$line" | cut -d ' ' -f9)

		# Do not process other statuses
		[ "$status" = 201 ] || [ "$status" = 204 ] || continue

		bin/startApp.sh "$app" &

		# Reset the log file accessible from the editor
		echo 'Truncate application log'
		truncate -s 0 "apps/$app/public/app.log"

		# This is a kind of plugin mechanism, to start "something else"
		[ -x "apps/$app/bin/apache_on_change.sh" ] &&
			"apps/$app/bin/apache_on_change.sh" "$line"

		# git synchronise the content using
		bin/synchroGit.sh "$app"
	done >>"log/app/$app.log" &
done

wait
