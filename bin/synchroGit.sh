#!/bin/sh

# Synchronise content with git repository

cd "$(dirname "$0")/.." || exit

# Application being managed
app="${1:-april.filsdu.net}"

cd "apps/$app/content" || exit

if [ "$(git status --porcelain)" ]; then
	echo "Something to commit..."

	# Drafts listing
	grep -ilrw "^draft:\s*true$" >drafts.txt
	[ ! -s drafts.txt ] && rm drafts.txt

	git add .
	msg="$(git diff -w -b HEAD | grep 'title: ')"
	[ -z "$msg" ] && msg="$(git status --porcelain)"
	git commit . -m "$msg"
	git push

else
	git pull
fi
