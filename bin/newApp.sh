#!/bin/sh

# Script to setup a new simple web application
# Two parameters:
# * app's name
# * theme's url

dir=/var/simpleWeb

toCreate=${1:-test}
theme=${2:-https://github.com/budparr/gohugo-theme-ananke.git}

# [ ! "$(command -v git)" ] && sudo apt install git

shortTheme=$(basename "$theme" .git)

newHugoSite() {
	echo "Creating new site called: $toCreate, with theme: $shortTheme"

	cd $dir/apps || exit
	hugo new site "$toCreate"

	cd "$toCreate" || exit

	git init
	git submodule add "$theme" "themes/$shortTheme"
	cp -ar "themes/$shortTheme/exampleSite/"* .
	sed -i '/themesDir.*/D' config.toml
	sed -i '/baseURL.*/D' config.toml

	echo "public" >>.gitignore

	git add .
	git ci . -m 'Initial version'

	chgrp www-data .
	chgrp -R www-data content
	chgrp -R www-data resources
	chmod g+w .
	chmod -R g+w . content
}

setupApache() {
	echo "Setting up apache configuration"

	cd $dir || exit
	cp conf/apache/sandbox.conf "conf/apache/$toCreate.conf"

	sed -i "s/sandbox/$toCreate/g" "conf/apache/$toCreate.conf"

	cp conf/apache/header.html "apps/$toCreate/content/.apache_header.html"
}

setupSudo() {
	sudo ln -s "$dir/conf/apache/$toCreate.conf" /etc/apache2/sites-available/
	sudo a2ensite "$toCreate.conf"
	sudo service apache2 restart

	sudo -u www-data hugo -s "$dir/apps/$toCreate"
}

[ ! -d "$dir/apps/$toCreate" ] && newHugoSite

[ ! -f "$dir/conf/apache/$toCreate.conf" ] && setupApache

[ ! -f "/etc/apache2/sites-available/$toCreate.conf" ] && setupSudo
